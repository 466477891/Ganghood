//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from: Chat.proto
namespace Message
{
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"CToSChat")]
  public partial class CToSChat : global::ProtoBuf.IExtensible
  {
    public CToSChat() {}
    
    private int _fromuid;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"fromuid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int fromuid
    {
      get { return _fromuid; }
      set { _fromuid = value; }
    }
    private string _fromuname;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"fromuname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string fromuname
    {
      get { return _fromuname; }
      set { _fromuname = value; }
    }
    private int _touid;
    [global::ProtoBuf.ProtoMember(3, IsRequired = true, Name=@"touid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int touid
    {
      get { return _touid; }
      set { _touid = value; }
    }
    private string _touname;
    [global::ProtoBuf.ProtoMember(4, IsRequired = true, Name=@"touname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string touname
    {
      get { return _touname; }
      set { _touname = value; }
    }
    private string _content;
    [global::ProtoBuf.ProtoMember(5, IsRequired = true, Name=@"content", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string content
    {
      get { return _content; }
      set { _content = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"SToCChat")]
  public partial class SToCChat : global::ProtoBuf.IExtensible
  {
    public SToCChat() {}
    
    private int _fromuid;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"fromuid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int fromuid
    {
      get { return _fromuid; }
      set { _fromuid = value; }
    }
    private string _fromuname;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"fromuname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string fromuname
    {
      get { return _fromuname; }
      set { _fromuname = value; }
    }
    private int _touid;
    [global::ProtoBuf.ProtoMember(3, IsRequired = true, Name=@"touid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int touid
    {
      get { return _touid; }
      set { _touid = value; }
    }
    private string _touname;
    [global::ProtoBuf.ProtoMember(4, IsRequired = true, Name=@"touname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string touname
    {
      get { return _touname; }
      set { _touname = value; }
    }
    private string _content;
    [global::ProtoBuf.ProtoMember(5, IsRequired = true, Name=@"content", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string content
    {
      get { return _content; }
      set { _content = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
}