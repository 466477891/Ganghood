﻿using SuperSocket.Common;
using SuperSocket.Facility.Protocol;
using SuperSocket.SocketBase.Protocol;
using System; 

namespace LoginServer.Net
{
    public class ReceiveFilter : FixedHeaderReceiveFilter<BinaryRequestInfo>
    {
        public ReceiveFilter() : base(8)
        {
        }

        protected override int GetBodyLengthFromHeader(byte[] header, int offset, int length)
        {
            var bodyData = new byte[4];
            Array.Copy(header, offset + 4, bodyData, 0, 4);
            return BitConverter.ToInt32(bodyData, 0);
        }

        protected override BinaryRequestInfo ResolveRequestInfo(ArraySegment<byte> header, byte[] bodyBuffer, int offset, int length)
        {
            byte[] p = new byte[4];
            Array.Copy(header.Array, p, 4);
            return new BinaryRequestInfo(BitConverter.ToInt32(p, 0).ToString(), bodyBuffer.CloneRange(offset, length));
        }
    }
}
