﻿using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginServer.Net
{
    public class GameServer : AppServer<PlayerSession, BinaryRequestInfo>
    {
        public GameServer() : base(new ReceiveFilterFactory()) { }

    }
}
