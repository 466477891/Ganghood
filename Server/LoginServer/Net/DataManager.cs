﻿using MySql.Data.MySqlClient;
using System; 
using System.Text.RegularExpressions; 

namespace LoginServer.Net
{
    public class DataMgr
    {
        MySqlConnection sqlConn;

        //单例模式
        public static DataMgr instance;
        public DataMgr()
        {
            instance = this;
            Connect();
        }

        //连接
        public void Connect()
        {
            //数据库
            string connStr = "server=211.159.159.200;user=Unity;";
            connStr += "database=ghol;password=Unity/521;port=3306";
            sqlConn = new MySqlConnection(connStr); 
            try
            { 
                //sqlConn.Open();
                //Console.WriteLine("[数据库]Connect " + "成功");
            }
            catch (Exception e)
            {
                Console.WriteLine("[数据库]Connect " + e.Message);
                return;
            }
        }  

        //判定安全字符串
        public bool IsSafeStr(string str)
        {
            return !Regex.IsMatch(str, @"[-|;|,|\/|\(|\)|\[|\]|\}|\{|%|@|\*|!|\']");
        }

        //是否存在该用户
        private bool CanRegister(string uname)
        { 
            //防sql注入
            if (!IsSafeStr(uname))
                return false;
            sqlConn.Open();
            //查询id是否存在
            string cmdStr = string.Format("select * from jh_user where uname='{0}';", uname);
            MySqlCommand cmd = new MySqlCommand(cmdStr, sqlConn);
            try
            { 
                MySqlDataReader dataReader = cmd.ExecuteReader();
                bool hasRows = dataReader.HasRows;
                dataReader.Close();
                sqlConn.Close();
                return !hasRows; 
            }
            catch (Exception e)
            {
                Console.WriteLine("[数据库]CanRegister fail " + e.Message);
                sqlConn.Close();
                return false;
            } 
        }

        //注册
        public bool Register(string uname, string pwd)
        { 
            //防sql注入
            if (!IsSafeStr(uname) || !IsSafeStr(pwd))
            {
                Console.WriteLine("[数据库]Register 使用非法字符");
                return false;
            }
            //能否注册
            if (!CanRegister(uname))
            {
                Console.WriteLine("[数据库]Register !CanRegister");
                return false;
            }
            sqlConn.Open();
            //写入数据库User表
            string cmdStr = string.Format("insert into jh_user set uname ='{0}' ,pwd ='{1}';", uname, pwd);
            MySqlCommand cmd = new MySqlCommand(cmdStr, sqlConn);
            try
            {
                cmd.ExecuteNonQuery();
                sqlConn.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("[数据库]Register " + e.Message);
                sqlConn.Close();
                return false;
            }
            
        }

        /// <summary>
        /// 检测用户名密码,返回uid 或-1
        /// </summary>
        /// <param name="uname"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public int CheckPassWord(string uname, string pwd)
        { 
            int uid = -1;
            //防sql注入
            if (!IsSafeStr(uname) || !IsSafeStr(pwd))
                return uid;
            sqlConn.Open();
            //查询
            string cmdStr = string.Format("select * from jh_user where uname='{0}' and pwd='{1}';", uname, pwd);
            MySqlCommand cmd = new MySqlCommand(cmdStr, sqlConn);
            try
            {
                MySqlDataReader dataReader = cmd.ExecuteReader();
                bool hasRows = dataReader.HasRows;
                if(hasRows)
                {
                    if(dataReader.Read())
                    {
                        uid = dataReader.GetInt32(0);
                    }

                }
                dataReader.Close();
                sqlConn.Close();
                return uid;
            }
            catch (Exception e)
            {
                Console.WriteLine("[数据库]CheckPassWord " + e.Message);
                sqlConn.Close();
                return uid;
            }
        }
          
    }
}
