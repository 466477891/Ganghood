﻿using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol; 
using System.Net; 

namespace LoginServer.Net
{
    public class ReceiveFilterFactory : IReceiveFilterFactory<BinaryRequestInfo>
    {
        public IReceiveFilter<BinaryRequestInfo> CreateFilter(IAppServer appServer, IAppSession appSession, IPEndPoint remoteEndPoint)
        {
            return new ReceiveFilter();
        }
    }
}
