//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from: ShareMsg.proto
namespace Message
{
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"CToSShareMsg")]
  public partial class CToSShareMsg : global::ProtoBuf.IExtensible
  {
    public CToSShareMsg() {}
    
    private int _myUid;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"myUid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int myUid
    {
      get { return _myUid; }
      set { _myUid = value; }
    }
    private string _myUname;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"myUname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string myUname
    {
      get { return _myUname; }
      set { _myUname = value; }
    }
    private string _content;
    [global::ProtoBuf.ProtoMember(3, IsRequired = true, Name=@"content", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string content
    {
      get { return _content; }
      set { _content = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"SToCShareMsg")]
  public partial class SToCShareMsg : global::ProtoBuf.IExtensible
  {
    public SToCShareMsg() {}
    
    private int _myUid;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"myUid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int myUid
    {
      get { return _myUid; }
      set { _myUid = value; }
    }
    private string _myUname;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"myUname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string myUname
    {
      get { return _myUname; }
      set { _myUname = value; }
    }
    private string _content;
    [global::ProtoBuf.ProtoMember(3, IsRequired = true, Name=@"content", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string content
    {
      get { return _content; }
      set { _content = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
}