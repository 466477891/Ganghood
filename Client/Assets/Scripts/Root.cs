﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Root : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        //后台运行
        Application.runInBackground = true;
        //限制帧率30
        Application.targetFrameRate = 30;
        Loom.Initialize();
        //ResMgr.mode = RESMODE.AB;
        ResMgr.mode = RESMODE.RES;
        ResMgr.resPathRoot = "UIPrefabs";

        gameObject.AddComponent<ResMgr>();
        gameObject.AddComponent<MsgMgr>();
        gameObject.AddComponent<NetMgr>();
        gameObject.AddComponent<PanelMgr>();
        //跳转场景不销毁
        DontDestroyOnLoad(gameObject);

        //PanelMgr.Instance.OpenPanel<LoginPanel>("");
        PanelMgr.Instance.OpenPanel<UpdatePanel>("");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
