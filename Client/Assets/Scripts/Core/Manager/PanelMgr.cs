﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 面板管理器
/// </summary>
public class PanelMgr : MonoBehaviour
{
    public static PanelMgr Instance;

    private string resourcesPath = "UIPrefabs";
    private GameObject canvas;

    public Dictionary<string, PanelBase> dict;

    private Dictionary<PanelLayer, Transform> layerDict;

    public void Awake()
    {
        Instance = this;
        InitLayer();
        dict = new Dictionary<string, PanelBase>();
    }

    private void InitLayer()
    {
        //画布
        canvas = GameObject.Find("Canvas");
        if (canvas == null)
        {
            Debug.LogError("panelMgr.InitLayer fail, canvas is null");
        }
        //各个层级
        layerDict = new Dictionary<PanelLayer, Transform>();

        foreach (PanelLayer pl in Enum.GetValues(typeof(PanelLayer)))
        {
            string name = pl.ToString();
            Transform transform = canvas.transform.Find(name);
            layerDict.Add(pl, transform);
        }
    }

    /// <summary>
    /// 打开面板
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="skinPath"></param>
    /// <param name="args"></param>
    public void OpenPanel<T>(string skinPath, params object[] args) where T : PanelBase
    {
        //已经打开
        string panelName = typeof(T).ToString();
        Debug.LogWarning("panelName:" + panelName);
        if (dict.ContainsKey(panelName))
        {
            PanelBase p = (PanelBase)dict[panelName];
            if (!p.panelObj.activeSelf)
            {
                p.panelObj.SetActive(true);
                //面板脚本
                p = canvas.GetComponent<T>();
                p.Init(args);
            }
            return;
        }
        //面板脚本
        PanelBase panel = canvas.AddComponent<T>();
        panel.Init(args);
        dict.Add(panelName, panel);

        panel.panelObj = (GameObject)Instantiate(ResMgr.Instance.Load(panelName));
        ResMgr.Instance.Unload("panelName", false);

        panel.panelObj.name = panelName;
        //坐标
        Transform skinTrans = panel.panelObj.transform;
        PanelLayer layer = panel.layer;
        Transform parent = layerDict[layer];
        skinTrans.SetParent(parent, false);
        //panel的生命周期
        panel.OnShowing();
        //anm
        panel.OnShowed();
    }

    //关闭面板
    public void ClosePanel(string name)
    {
        if (!dict.ContainsKey(name))
        {
            return;
        }
        PanelBase panel = (PanelBase)dict[name];
        if (panel == null)
        {
            return;
        }
        panel.OnClosing();
        dict.Remove(name);
        panel.OnClosed();
        GameObject.Destroy(panel.panelObj);
        Component.Destroy(panel);
    }

    public void ActivePanel(string name)
    {
        PanelBase panel = (PanelBase)dict[name];
        if (panel == null)
        {
            return;
        }
        panel.panelObj.SetActive(true);
    }

    public void DisablePanel(string name)
    {
        PanelBase panel = (PanelBase)dict[name];
        if (panel == null)
        {
            return;
        }
        panel.panelObj.SetActive(false);
    }
}