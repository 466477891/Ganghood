﻿using Message;
using System;
using System.Collections.Generic;

public class ProtoDic
{
    private static List<int> _protoId = new List<int>
    {
            1001,
            1002,

            1003,
            1004,

            1005,
            1006,

            1007,
            1008,

            1009,
            1010,

            1011,
            1012,

            1013,
            1014,

            1015,
            1016,

            1017,
            1018,

            1019,
            1020,

            1021,
            1022,
     };

    private static List<Type> _protoType = new List<Type>
    {
            typeof(CToSRegister),
            typeof(SToCRegisterRet),

            typeof(CToSLogin),
            typeof(SToCLoginRet),

            typeof(CToSHeatBeat),
            typeof(SToCHeatBeat),

            typeof(CToSLogout),
            typeof(SToCLogoutRet),

            typeof(CToSChat),
            typeof(SToCChat),

            typeof(CToSAddFriend),
            typeof(SToCAddFriend),

            typeof(CToSAddFriendRet),
            typeof(SToCAddFriendRet),

            typeof(CToSDelFriend),
            typeof(SToCDelFriend),

            typeof(CToSDelFriendRet),
            typeof(SToCDelFriendRet),

            typeof(CToSGetPlayerList),
            typeof(SToCGetPlayerList),

            typeof(CToSMove),
            typeof(SToCMoves),

    };

    public static Type GetProtoTypeByProtoId(int protoId)
    {
        int index = _protoId.IndexOf(protoId);
        return _protoType[index];
    }

    public static int GetProtoIdByProtoType(Type type)
    {
        int index = _protoType.IndexOf(type);
        return _protoId[index];
    }

    public static bool ContainProtoId(int protoId)
    {
        if (_protoId.Contains(protoId))
        {
            return true;
        }
        return false;
    }

    public static bool ContainProtoType(Type type)
    {
        if (_protoType.Contains(type))
        {
            return true;
        }
        return false;
    }
}