//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from: AddFriend.proto
namespace Message
{
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"CToSAddFriend")]
  public partial class CToSAddFriend : global::ProtoBuf.IExtensible
  {
    public CToSAddFriend() {}
    
    private int _fromuid;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"fromuid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int fromuid
    {
      get { return _fromuid; }
      set { _fromuid = value; }
    }
    private string _fromuname;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"fromuname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string fromuname
    {
      get { return _fromuname; }
      set { _fromuname = value; }
    }
    private int _touid;
    [global::ProtoBuf.ProtoMember(3, IsRequired = true, Name=@"touid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int touid
    {
      get { return _touid; }
      set { _touid = value; }
    }
    private string _touname;
    [global::ProtoBuf.ProtoMember(4, IsRequired = true, Name=@"touname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string touname
    {
      get { return _touname; }
      set { _touname = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"SToCAddFriend")]
  public partial class SToCAddFriend : global::ProtoBuf.IExtensible
  {
    public SToCAddFriend() {}
    
    private int _fromuid;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"fromuid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int fromuid
    {
      get { return _fromuid; }
      set { _fromuid = value; }
    }
    private string _fromuname;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"fromuname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string fromuname
    {
      get { return _fromuname; }
      set { _fromuname = value; }
    }
    private int _touid;
    [global::ProtoBuf.ProtoMember(3, IsRequired = true, Name=@"touid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int touid
    {
      get { return _touid; }
      set { _touid = value; }
    }
    private string _touname;
    [global::ProtoBuf.ProtoMember(4, IsRequired = true, Name=@"touname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string touname
    {
      get { return _touname; }
      set { _touname = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"CToSAddFriendRet")]
  public partial class CToSAddFriendRet : global::ProtoBuf.IExtensible
  {
    public CToSAddFriendRet() {}
    
    private int _fromuid;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"fromuid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int fromuid
    {
      get { return _fromuid; }
      set { _fromuid = value; }
    }
    private string _fromuname;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"fromuname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string fromuname
    {
      get { return _fromuname; }
      set { _fromuname = value; }
    }
    private int _touid;
    [global::ProtoBuf.ProtoMember(3, IsRequired = true, Name=@"touid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int touid
    {
      get { return _touid; }
      set { _touid = value; }
    }
    private string _touname;
    [global::ProtoBuf.ProtoMember(4, IsRequired = true, Name=@"touname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string touname
    {
      get { return _touname; }
      set { _touname = value; }
    }
    private int _code;
    [global::ProtoBuf.ProtoMember(5, IsRequired = true, Name=@"code", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int code
    {
      get { return _code; }
      set { _code = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
  [global::System.Serializable, global::ProtoBuf.ProtoContract(Name=@"SToCAddFriendRet")]
  public partial class SToCAddFriendRet : global::ProtoBuf.IExtensible
  {
    public SToCAddFriendRet() {}
    
    private int _fromuid;
    [global::ProtoBuf.ProtoMember(1, IsRequired = true, Name=@"fromuid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int fromuid
    {
      get { return _fromuid; }
      set { _fromuid = value; }
    }
    private string _fromuname;
    [global::ProtoBuf.ProtoMember(2, IsRequired = true, Name=@"fromuname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string fromuname
    {
      get { return _fromuname; }
      set { _fromuname = value; }
    }
    private int _touid;
    [global::ProtoBuf.ProtoMember(3, IsRequired = true, Name=@"touid", DataFormat = global::ProtoBuf.DataFormat.TwosComplement)]
    public int touid
    {
      get { return _touid; }
      set { _touid = value; }
    }
    private string _touname;
    [global::ProtoBuf.ProtoMember(5, IsRequired = true, Name=@"touname", DataFormat = global::ProtoBuf.DataFormat.Default)]
    public string touname
    {
      get { return _touname; }
      set { _touname = value; }
    }
    private global::ProtoBuf.IExtension extensionObject;
    global::ProtoBuf.IExtension global::ProtoBuf.IExtensible.GetExtensionObject(bool createIfMissing)
      { return global::ProtoBuf.Extensible.GetExtensionObject(ref extensionObject, createIfMissing); }
  }
  
}