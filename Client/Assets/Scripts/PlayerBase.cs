﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBase : MonoBehaviour
{
    /// <summary>
    /// 移动速度
    /// </summary>
    private float moveSpeed;
    private float x;
    private float y;
    private float z; 
    private float angle;
    private Vector3 vecMove;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        vecMove = new Vector3(x, 0, y) * Time.deltaTime * moveSpeed / 10;
        transform.localPosition += vecMove;
        angle = Mathf.Atan2(x, y) * Mathf.Rad2Deg - 10;
        transform.localRotation = Quaternion.Euler(Vector3.up * angle);
    }
}
