﻿using Message;
using UnityEngine;
using UnityEngine.UI;

public class MainPanel : PanelBase
{
    private Button getPlayerListBtn;
    private GameObject playerBtnPrefab;
    private Joystick joystick;
    public override void Init(params object[] args)
    {
        base.Init(args); 
        layer = PanelLayer.Panel;
    }

    public override void OnShowing()
    {
        base.OnShowing();
        Transform skinTrans = panelObj.transform;
        getPlayerListBtn = skinTrans.Find("GetPlayerListBtn").GetComponent<Button>();
        joystick = skinTrans.Find("Joystick").GetComponent<Joystick>();
        getPlayerListBtn.onClick.AddListener(OnGetPlayerList);
        GameObject obj = ResMgr.Instance.Load("player");
        ResMgr.Instance.Unload("player",false);
        GameObject player = Instantiate(obj, Vector3.zero,Quaternion.identity);
        player.name = GlobeData.uid.ToString();
        player.transform.Find("Text").GetComponent<TextMesh>().text = GlobeData.uname;
        PlayerMoveControl pc = player.AddComponent<PlayerMoveControl>();
        pc.joystick = joystick;
    }

    void OnGetPlayerList()
    {
        //注册登录返回结果回调
        MsgMgr.Instance.AddListener(typeof(SToCGetPlayerList).ToString(), OnGetPlayerListBack);

        //发送
        CToSGetPlayerList protocol = new CToSGetPlayerList();
        protocol.type = 0;
        NetMgr.Instance.Send(protocol);
    }

    void OnGetPlayerListBack(object proto)
    {
        SToCGetPlayerList stcgl = proto as SToCGetPlayerList;
        if(stcgl.players.Count>0)
        {
            for (int i = 0; i < stcgl.players.Count; i++)
            {
                Debug.Log("uid:" + stcgl.players[i].uid + " uname:" + stcgl.players[i].uname);
            }
        }
        MsgMgr.Instance.RemoveListener(typeof(SToCGetPlayerList).ToString(), OnGetPlayerListBack);
    }
}
