﻿using UnityEngine;
using UnityEngine.UI;

public class TipPanel : PanelBase
{
    private Text infoText;
    private Button two_okBtn;
    private Button two_cancelBtn;
    private Button one_okBtn;
    private Transform twoBtn;
    private Transform oneBtn;
    string str = "";

    #region 生命周期

    //初始化
    public override void Init(params object[] args)
    {
        base.Init(args); 
        layer = PanelLayer.Tips;
        //参数 args[1]代表提示的内容
        if (args.Length == 1)
        {
            str = (string)args[0];
        }
    }

    //显示之前
    public override void OnShowing()
    {
        base.OnShowing();
        Transform skinTrans = panelObj.transform;

        twoBtn = skinTrans.Find("bgImage/twoButton");
        oneBtn = skinTrans.Find("bgImage/oneButton");

        //文字
        infoText = skinTrans.Find("infoText").GetComponent<Text>();
        infoText.text = str;
        //关闭按钮
        one_okBtn = oneBtn.Find("okButton").GetComponent<Button>();
        one_okBtn.onClick.AddListener(OnBtnClick);

        two_okBtn = twoBtn.Find("okButton").GetComponent<Button>();
        two_cancelBtn = twoBtn.Find("cancelButton").GetComponent<Button>();
    }
    #endregion

    //按下“知道了”按钮的事件
    public void OnBtnClick()
    {
        Close();
    }
}
