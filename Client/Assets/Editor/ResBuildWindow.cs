﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System;

/// <summary>
/// 资源打包工具窗口
/// </summary>
public class ResBuildWindow : EditorWindow
{
    static ResBuildWindow buildWindow;

    /// <summary>
    /// 资源平台列表
    /// </summary>
    static List<string> platformStrings = new List<string>();
    /// <summary>
    /// 当前资源平台索引
    /// </summary>
    static int nowPlatformIndex = 0;

    /// <summary>
    /// 是否处于焦点状态
    /// </summary>
    static bool isFocused = false;
    /// <summary>
    /// 数据是否加载
    /// </summary>
    static bool isLoadData = false;

    /// <summary>
    /// 预制体路径
    /// </summary>
    static string prefabPath;

    /// <summary>
    /// ab文件的根目录
    /// </summary>
    static string abPathRoot;

    /// <summary>
    /// AB文件路径
    /// </summary>        
    static string abPath;

    [MenuItem("工具/资源打包工具")]
    static void Init()
    {
        buildWindow = (ResBuildWindow)EditorWindow.GetWindow(typeof(ResBuildWindow), false, "打包工具", true);
        buildWindow.Show();
        LoadData();
    }

    /// <summary>
    /// 获得焦点
    /// </summary>
    private void OnFocus()
    {
        isFocused = true;
        if (!isLoadData)
        {
            LoadData();
        }
    }

    /// <summary>
    /// 丢失焦点
    /// </summary>
    private void OnLostFocus()
    {
        isFocused = false;
        isLoadData = false;
    }

    /// <summary>
    /// 加载数据
    /// </summary>
    static void LoadData()
    {
        prefabPath = Application.dataPath + "/Resources/UIPrefabs";
        abPathRoot = Application.persistentDataPath + "/AB/";

        if (Directory.Exists(abPathRoot))
        {
            Directory.CreateDirectory(abPathRoot);
        }

        platformStrings.Clear();
        platformStrings.Add(BuildTarget.Android.ToString());
        platformStrings.Add(BuildTarget.iOS.ToString());
        platformStrings.Add(BuildTarget.StandaloneWindows.ToString());
        platformStrings.Add(BuildTarget.WebGL.ToString());
        //platformStrings.Add(BuildTarget.StandaloneOSXUniversal.ToString()); 

        isLoadData = true;
    }

    void OnGUI()
    {
        if (!isFocused)
        {
            return;
        }

        EditorGUILayout.Space();
        if (GUILayout.Button("设置资源的AB名"))
        {
            SetAllAbNames();
        }
        EditorGUILayout.Space();

        if (GUILayout.Button("打印所有的AB包名字"))
        {
            PrintABsNames();
        }

        EditorGUILayout.Space();
        if (GUILayout.Button("清除所有AB包的名字"))
        {
            ClearABsName();
        }

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        GUILayout.Label("选择打包平台");
        nowPlatformIndex = GUILayout.Toolbar(nowPlatformIndex, platformStrings.ToArray());
        EditorGUILayout.Space();

        if (GUILayout.Button("打包所选平台资源"))
        {
            EditorUtility.DisplayProgressBar("打包所选平台资源", "打包所选平台资源", 0);
            BuildTarget bt = (BuildTarget)Enum.Parse(typeof(BuildTarget), platformStrings[nowPlatformIndex]);
            string abPath = abPathRoot + GetPlatformName(bt) + "/";
            if (!Directory.Exists(abPath))
            {
                Directory.CreateDirectory(abPath);
            }
            DirectoryInfo di = new DirectoryInfo(abPath);
            foreach (FileInfo fi in di.GetFiles())
            {
                fi.Delete();
            }
            //BuildAssetBundleOptions 
            BuildPipeline.BuildAssetBundles(abPath, BuildAssetBundleOptions.None, bt);
            CreateABMD5List.Execute(abPath);
            EditorUtility.ClearProgressBar();
        }

        EditorGUILayout.Space();
        if (GUILayout.Button("打包所有平台资源"))
        {
            EditorUtility.DisplayProgressBar("打包所有平台资源", "打包所有平台资源", 0);
            for (int i = 0; i < platformStrings.Count; i++)
            {
                BuildTarget bt = (BuildTarget)Enum.Parse(typeof(BuildTarget), platformStrings[i]);
                string abPath = abPathRoot + GetPlatformName(bt) + "/";
                if (!Directory.Exists(abPath))
                {
                    Directory.CreateDirectory(abPath);
                }
                DirectoryInfo di = new DirectoryInfo(abPath);
                foreach (FileInfo fi in di.GetFiles())
                {
                    fi.Delete();
                }
                BuildPipeline.BuildAssetBundles(abPath, BuildAssetBundleOptions.None, bt);
                CreateABMD5List.Execute(abPath);
            }
            EditorUtility.ClearProgressBar();
        }
        EditorGUILayout.Space();
    }

    /// <summary>
    /// 设置资源的AB名，这里可以处理资源文件的依赖关系，比如可以把一个文件的图片打成一个ab包，各种别的选择……
    /// </summary>
    void SetAllAbNames()
    {
        EditorUtility.DisplayProgressBar("设置预制的AB名", "设置预制的AB名", 0);

        DirectoryInfo folder = new DirectoryInfo(prefabPath);

        foreach (FileInfo file in folder.GetFiles("*.prefab"))
        {
            AssetImporter importer = AssetImporter.GetAtPath("Assets/Resources/UIPrefabs/" + file.Name);
            //设置ab名
            importer.assetBundleName = Util.GetFileRealName(file.Name.ToLower());
            //设置ab文件的后缀
            importer.assetBundleVariant = "t";
        }
        AssetDatabase.Refresh();
        EditorUtility.ClearProgressBar();
    }

    /// <summary>
    /// 打印所有的AB包名字
    /// </summary>
    void PrintABsNames()
    {
        EditorUtility.DisplayProgressBar("打印所有的AB包名字", "打印所有的AB包名字", 0);
        //获取所有设置的AssetBundle 
        string[] names = AssetDatabase.GetAllAssetBundleNames();
        foreach (string name in names)
        {
            Debug.Log("AB名: " + name);
        }
        AssetDatabase.Refresh();
        EditorUtility.ClearProgressBar();
    }

    /// <summary>
    /// 清除所有AB包的名字
    /// </summary>
    void ClearABsName()
    {
        EditorUtility.DisplayProgressBar("清除", "清除所有AB包的名字", 0);
        string[] ABsNames = AssetDatabase.GetAllAssetBundleNames();
        for (int j = 0; j < ABsNames.Length; j++)
        {
            AssetDatabase.RemoveAssetBundleName(ABsNames[j], true);
        }
        //刷新资源
        AssetDatabase.Refresh();
        EditorUtility.ClearProgressBar();
    }
     
    /// <summary>
    /// 获取平台名
    /// </summary>
    /// <param name="bt"></param>
    /// <returns></returns>
    private static string GetPlatformName(BuildTarget bt)
    {
        switch (bt)
        {
            case BuildTarget.Android:
            case BuildTarget.iOS:
                return bt.ToString();
            case BuildTarget.StandaloneWindows:
            case BuildTarget.StandaloneWindows64:
                return "Windows";
            case BuildTarget.WebGL:
                return "WebGL";
            case BuildTarget.StandaloneOSXIntel:
            case BuildTarget.StandaloneOSXIntel64:
            case BuildTarget.StandaloneOSXUniversal:
                return "OSX";
            default:
                Debug.LogError("不支持的打包渠道");
                return "";
        }
    }
}
