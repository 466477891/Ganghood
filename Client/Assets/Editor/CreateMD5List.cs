﻿using UnityEditor;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System;
using UnityEngine;
/// <summary>
/// 创建AB文件的列表xml
/// </summary>
public class CreateABMD5List
{
    public static void Execute(BuildTarget target)
    {
        Execute(target);
        AssetDatabase.Refresh();
    }

    public static void Execute(string path)
    {
        VersionInfo versionInfo = new VersionInfo();
        versionInfo.content = new List<UpdateContent>();
        DirectoryInfo folder = new DirectoryInfo(path);

        foreach (FileInfo file in folder.GetFiles())
        {
            //if (file.Name.Contains("Android")|| file.Name.Contains("iOS")|| file.Name.Contains("Win"))
            //{
            //    File.Delete(file.FullName);
            //    continue;
            //}
            //if (file.Name.Contains(".manifest"))
            //{
            //    File.Delete(file.FullName);
            //    continue;
            //}
            if (file.Name.Contains(".DS_Store") || file.Name.Contains(".meta") || file.Name.Contains(".txt"))
            {
                continue;
            }
            if (!Path.GetFileName(file.FullName).Contains("."))
            {

                file.MoveTo(file.FullName + ".t");
            }

            UpdateContent ui = new UpdateContent();
            ui.name = Path.GetFileName(file.FullName);
            ui.md5 = Util.GetFileMD5(file.FullName);
            versionInfo.content.Add(ui);
        }
        // 删除前一版的old数据
        if (File.Exists(path + "/VersionMD5-old.txt"))
        {
            File.Delete(path + "/VersionMD5-old.txt");
        }

        // 如果之前的版本存在，则将其名字改为VersionMD5-old.txt
        if (File.Exists(path + "/VersionMD5.txt"))
        {
            File.Move(path + "/VersionMD5.txt", path + "/VersionMD5-old.txt");
        }

        // 读取旧版本的MD5
        VersionInfo oldVersionInfo = ReadMD5File(path + "/VersionMD5-old.txt");

        if (File.Exists(path + "/VersionMD5-old.txt"))
        {
            File.Delete(path + "/VersionMD5-old.txt");
        }

        XmlDocument XmlDoc = new XmlDocument();
        XmlElement XmlRoot = XmlDoc.CreateElement("Files");
        XmlDoc.AppendChild(XmlRoot);

        XmlElement ver = XmlDoc.CreateElement("Version");
        XmlRoot.AppendChild(ver);
        if (oldVersionInfo.version > 0)
        {
            ver.SetAttribute("code", oldVersionInfo.version + 1 + "");
        }
        else
        {
            ver.SetAttribute("code", 1 + "");
        }

        for (int i = 0; i < versionInfo.content.Count; i++)
        {
            XmlElement xmlElem = XmlDoc.CreateElement("File");
            XmlRoot.AppendChild(xmlElem);

            xmlElem.SetAttribute("name", versionInfo.content[i].name);
            xmlElem.SetAttribute("md5", versionInfo.content[i].md5);
        }


        //oldVersionInfo中有，而versionInfo中没有的信息，手动添加到versionInfo
        foreach (UpdateContent oui in oldVersionInfo.content)
        {
            if (!versionInfo.content.Contains(oui))
            {
                versionInfo.content.Add(oui);
            }
        }
        XmlDoc.Save(path + "/VersionMD5.txt");
        XmlDoc = null;
    }
    /// <summary>
    /// 读取文件md5列表
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public static VersionInfo ReadMD5File(string fileName)
    {
        VersionInfo versionInfo = new VersionInfo();
        versionInfo.content = new List<UpdateContent>();
        versionInfo.version = 0;
        try
        { 
            // 如果文件不存在，则直接返回
            if (File.Exists(fileName) == false)
            {
                return versionInfo;
            }
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(fileName);
            XmlElement XmlRoot = XmlDoc.DocumentElement;

            foreach (XmlNode node in XmlRoot.ChildNodes)
            {
                //Debug.Log("node.Name:" + node.Name);
                if (node.Name == "File")
                {
                    if ((node is XmlElement) == false)
                    {
                        continue;
                    }
                    UpdateContent ui = new UpdateContent();
                    ui.name = (node as XmlElement).GetAttribute("name");
                    ui.md5 = (node as XmlElement).GetAttribute("md5");
                    versionInfo.content.Add(ui);
                }
                else
                {
                    versionInfo.version = Convert.ToInt32((node as XmlElement).GetAttribute("code"));
                }
            }

            XmlRoot = null;
            XmlDoc = null;
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }
        return versionInfo;
    }

    /// <summary>
    /// 更新的版本信息
    /// </summary>
    public struct VersionInfo
    {
        /// <summary>
        /// 版本号
        /// </summary>
        public int version;
        /// <summary>
        /// 更新的内容
        /// </summary>
        public List<UpdateContent> content;
    }

    /// <summary>
    /// 更新内容信息
    /// </summary>
    public struct UpdateContent
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string name;
        /// <summary>
        /// md5
        /// </summary>
        public string md5;
    }
}
