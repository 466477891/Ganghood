# Ganghood

#### 项目介绍
Ganghood
使用Unity开发一款极简网络游戏，前端Unity、C#，后端也是C#（就会这么点）、SupserSocket，协议用Protobuf，数据库MySql，功能或许会少的可怜……

#### Unity软件版本
当前使用Unity 2017.2.2f1 (64-bit)

#### 使用说明

1. 下载工程
2. 打开Unity运行（Unity版本不一致会导致加载资源出现问题）
3. 服务器端在腾讯云上运行中，目前有简单的注册、登录和位置同步的功能

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 当前遇到的未解决的问题（欢迎留言一起讨论）

1. 服务端自定义的AppSession的OnSessionClosed无法触发 https://github.com/kerryjiang/SuperSocket/issues/147
2. 项目中使用了Loom插件，但WebGL不能使用多线程，导致GG